'use strict'

import VueSpinner from '@/components/common/spinner/VueSpinner'
import Vue from 'vue'

let Spinner = {
    $vm: null,
    init(vm) {this.$vm = vm},
    start() {
        this.$vm.active = true;
    },
    end() {
        this.$vm.active = false;
    }

}

const vueProgressEventBus = new Vue({
    data: {
        active: false
    }
})

window.progressEventBus = vueProgressEventBus;

Spinner.init(vueProgressEventBus);

Vue.component('vue-spinner', VueSpinner);

export { Spinner };
