import Vue from 'vue'
import App from './App'
import router from './router'
//import axios from './backend/vue-axios'
import store from './store'
import VueLazyload from 'vue-lazyload'
import VModal from 'vue-js-modal'
import VueFriendlyIframe from 'vue-friendly-iframe';

import {api, http, apiContext} from './backend/vue-axios'
import { Carousel, Slide } from 'vue-carousel'
import { Spinner } from './components/common/Spinner'

import {VueMasonryPlugin} from 'vue-masonry';

Vue.use(VueMasonryPlugin)
Vue.use(VueLazyload, {
    lazyComponent: true
});
Vue.use(VModal, {dynamic: true, injectModalsContainer: true ,dialog: true})
//debug for.. 
Vue.config.productionTip = true

Vue.component('carousel', Carousel);
Vue.component('slide', Slide);
Vue.component('vue-friendly-iframe', VueFriendlyIframe);

Vue.prototype.$Spinner = Spinner;

Vue.prototype.$api = api;
Vue.prototype.$http = http;

apiContext.router = router;
apiContext.store = store;
apiContext.spinner = Spinner;
// dynamic binding for mdl : hook inserted
Vue.directive('mdl', {
    inserted: function(el) {
        componentHandler.upgradeElements(el);
    }
});

var EventBus = new Vue();
Object.defineProperties(Vue.prototype, {
    $eventBus: {
        get: function () {
            return EventBus;
        }
    }
});

//attaches router, axios to our instance 
export default new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }  
})
