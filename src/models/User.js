import JwtDecode from 'jwt-decode'

export default class User {
  static from (token) {
    try {
      let obj = JwtDecode(token)
      return new User(obj)
    } catch (_) {
      return null
    }
  }

  constructor ({ id, name, exp }) {
    this.id = id // eslint-disable-line camelcase
    this.name = name
    this.exp = exp
    this.picture = '/static/user/' + id + '.jpeg';
  }

}