import {router} from '../index'
import store from '../store/index'
import * as MutationTypes from '../store/mutation-types'

//login 서비스 페이지 호출
//todo 일반 js에서 router객체를 접근할 수 있을까?
export default {
    login() {
        router.go('/login');
    },
    logout() {
        store.dispatch(MutationTypes.LOGOUT);
        router.go('/');
    },
    requireAuth(to, from, next) {

        if (store.getters.currentUser == null ) {
            next({
            path: '/login',
            query: { redirect: to.fullPath }
        });
      } else {
        next();
      }
    }
}
