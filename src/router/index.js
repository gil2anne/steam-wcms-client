'use strict';
import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'
//@ --> src
import Home from '@/components/Home'
import Login from '@/components/user/Login'
import SignUp from '@/components/user/SignUp'
import Auth from '@/components/user/Auth'
import IdeaPage from '@/components/IdeaPage'
import IdeaEdit from '@/components/IdeaEdit'
import RecommendPage from '@/components/RecommendPage'
import PageNotFound from '@/components/error/PageNotFound'

Vue.use( Router )

const routes = [
  { path: '/', name: 'home', component: Home },
  { path: '/login', name: 'login', component: Login },
  { path: '/auth', name: 'auth', component: Auth, props: (route) =>({token:route.query.token, refreshToken:route.query.refreshToken}) },
  { path: '/signup', name: 'signup', component: SignUp},
  { path: '/idea/:id', name: 'idea', component: IdeaPage, props: true, meta: { transitionName: 'hide'} },
  { path: '/recommend/:id', name: 'recommend', component: RecommendPage, meta: { transitionName: 'slide-left'}},
  { path: '/edit/idea', name: 'edit', component: IdeaEdit, props: (route) => ({contentId:route.query.id}), meta: { transitionName: 'slide-left' } },
  { path: '*', component: PageNotFound }
  
]

const vueRouter = new Router({
  mode : 'history',
  scrollBehavior:()=>({ x: 0, y: 0 }),
  routes : routes
});

vueRouter.beforeEach(function (to, from, next){
  const nextRoute = ['/edit/idea']

  if(nextRoute.indexOf(to.path) >= 0){

    if(!store.getters.isLogin){
      next({path:'/login'})
      return;
    }
  }   
  next()
}); 

export default vueRouter;
