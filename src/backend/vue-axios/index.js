import axios from 'axios'
import * as TokenTypes from '../../api/token_types'

const AUTH_URL = process.env.AUTH_URL || 'http://localhost:8090/auth'
const API_URL = process.env.API_URL || 'http://localhost:8090/api'
const HTTP_URL = process.env.HTTP_URL || 'http://localhost:8090/'

let http = axios.create({
  baseURL : HTTP_URL,
  timeout : 100000,
  headers: {
    'Content-Type': 'application/json'
  }
})

let api = axios.create({
  baseURL : API_URL,
  timeout : 100000,
  headers: {
    'Content-Type': 'application/json'
  }
});

api.interceptors.request.use((config) => {
  
  const token = localStorage.getItem(TokenTypes.TOKEN);

  if( token ) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  apiContext.spinner.start();
  if ( apiContext.store.getters.tokenIsExpired && !config._retry) {
    const originalRequest = config;
    originalRequest._retry = true;
    return issueToken(originalRequest);
  }
  return config;
}, (err) => {
  apiContext.spinner.end();
  return Promise.reject(err.response)
})


api.interceptors.response.use((response) => response, (error) => {
 
  const originalRequest = error.config;
  
  if( error.response ) {  
    let response = error.response;

    if( response.status == 403 ) {
      apiContext.store.commit('LOGOUT');
      apiContext.router.replace({path:"/login"});
    } else {
      let errorCode = response.data.errorCode | response.data.errorResponse.errorCode;
      switch(errorCode ) {
        case 11 : //token expired
          return issueToken(originalRequest);
        case 10 : 
          apiContext.store.commit('LOGOUT');
          apiContext.router.replace({path:"/login"});
      }
    }      
  }
  apiContext.spinner.end();

  return Promise.reject(error);
});

function issueToken(originalRequest) {
  originalRequest._retry = true;
  const refreshToken = window.localStorage.getItem(TokenTypes.REFRESH_TOKEN);
  return new Promise((resolve, reject) => {
    return axios.post(AUTH_URL + '/refresh', { refreshToken })
    .then(({data}) => {
      apiContext.store.commit('LOGIN', data);
      return api(originalRequest);
    }).catch((err) => {
      if( err.response ) {
        switch(err.response.status ) {
          case 401 : 
          apiContext.store.commit('LOGOUT');
          apiContext.router.replace({path:"/login"});
          break;
          case 500 : 
          break;
        }
      }
      return reject(err);
    });
  });
}
const apiContext = { store : null, router: null}

export { api, http, apiContext }
