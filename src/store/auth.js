/* global localStorage */


import User from '@/models/User'
import JwtDecode from 'jwt-decode'

import * as MutationTypes from './mutation-types'
import * as TokenTypes from '../api/token_types'

const state = {
    user: User.from(localStorage.getItem(TokenTypes.TOKEN))
}
  
const mutations = {
    [MutationTypes.LOGIN] (state, payload) {
        
        localStorage.setItem(TokenTypes.TOKEN, payload.token);
        localStorage.setItem(TokenTypes.REFRESH_TOKEN, payload.refreshToken);
        state.user = User.from(payload.token)
    },
    [MutationTypes.LOGOUT] (state) {
        state.user = null
        localStorage.clear();
    }
}

const getters = {
    isLogin(state) {
        return state.user != null;
    },
    currentUser (state) {
        return state.user;// | new User({id:'',name:'', exp:''})
    },
    tokenIsExpired(state) {
        const now = Date.now().valueOf() / 1000
        return  state.user != null && state.user.exp < now 
    }
}

const actions = {
    login ({ commit }) {
        commit(MutationTypes.LOGIN)
    },

    logout ({ commit }) {
        commit(MutationTypes.LOGOUT)
    }
}

export default {
    state,
    mutations,
    getters,
    actions
}